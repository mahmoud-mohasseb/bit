import { DocsAspect } from './docs.aspect';

export type { DocsMain } from './docs.main.runtime';
export type { DocsUI } from './docs.ui.runtime';
export type { DocsPreview } from './docs.preview.runtime';
export type { DocReader } from './doc-reader';
export { Doc, DocProp, DocPropList } from './doc';

export type { TitleBadgeSlot } from './overview';

export { DocsAspect };
export default DocsAspect;
